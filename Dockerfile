FROM node:10
MAINTAINER ADMIN <ollita7@gmail.com>

RUN mkdir /workshop
WORKDIR /workshop

# install 
COPY . /workshop
RUN npm i
RUN npm i -g kiwi-server-cli
RUN npm install typescript -g
RUN kc build

CMD ["entrypoint.sh"]
EXPOSE 8086
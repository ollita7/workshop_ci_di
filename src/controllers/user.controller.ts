import { JsonController, Get, Post, Param, Body} from 'kiwi-server';

@JsonController('/user')
export class userController {

    @Get('/get/:id')
    public get(@Param('id') id: string){
        return {
            id: id
        }
    }

    @Post('/post')
    public post(@Body() body: any){
        return body;
    }
}